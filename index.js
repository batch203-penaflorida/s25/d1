// JSON, or JavaScript Object Notation, is a data format used by applications to store and transport data to one another.

// JavaSript in it's name use for JSON is not limited to JavaScript - based applications and can also be used in other programming languages.

/* 
   Files that store JSON data are saved with a file extension of .json.
*/

/* 
  Below is an example of a JSON 
*/

/* 
   {
   "to": "Tove",
   "from" : "Jani",
   "heading": "Reminder",
   "body" : "Don't forget me this weekend!"
   }
*/
// console.log("Hello World");

// [SECTION] JSON Objects

/* 
   - JSON stands for JavaScript Object Notation
   - A common use of JSON is to read data from a web server, and display the data in a web page.
   - Feature of JSON:
      - It is a lightweight data-interchange format.
      - It is easy to read and write.
      - It is easy for machines to parse and generate.
*/
/* 
JSON Objects
   - JSON is alo use the "key/value pairs" just like the object properties in JavaScript.
   - "Key/properties" names requires to be enclosed with double quotes.
*/
/* 
   Syntax:
   {
      "propertyA" : "valueA",
      "propertyB" : "valueB"
   }
*/

// "cities" : {
//    "city" : "Quezon City",
//    "province" : "Metro Manila",
//    "country" : "Philippines"
// },
// {
//    "city" : "Manila City",
//    "province" : "Metro Manila",
//    "country" : "Philippines"
//    }

// JSON Arrays
// Arrays in JSON are almost same as arrays in javascript
// Arrays of JSON Object

// [SECTION] JSON Methods

// Converting Data Into Stringified JSON
// JavaScript Array of objects
let batchesArr = [
  {
    batchName: "Batch 203",
    schedule: "Full Time",
  },
  {
    batchName: "Batch 204",
    schedule: "Part Time",
  },
];

console.log(batchesArr);
console.log("Result from stringify method: ");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
  name: "John",
  age: 31,
  address: {
    city: "Manila",
    country: "Philippines",
  },
});
console.log(data);

// User details
// let firstName = prompt("Enter your first name: ");
// let lastName = prompt("Enter your last name: ");
// let email = prompt("Enter your email");
// let password = prompt("Enter your password");

// let otherData = JSON.stringify({
//   firstName: firstName,
//   lastName: lastName,
//   email: email,
//   password: password,
// });
// console.log(otherData);

// [SECTION] Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[
  {
    batchName: "Batch 203",
    schedule: "Full Time",
  },
  {
    batchName: "Batch 204",
    schedule: "Part Time",
  },
]`;

console.log("batchesJSON content:");
console.log(batchesJSON);

// console.log("Result from parse method:");
// let parseBatches = JSON.parse(batchesJSON);
// console.log(parseBatches[0].batchName);

let stringifiedObject = `{
   "name":"John",
   "age":31,
   "address":{
      "city" : "Manila",
      "country" : "Philippines"
   }
}`;

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));

// parse - from json object to javascript
// stringify - from javascript to json
